import numpy as np
import pdb


class Vertex:

    all_vertices = []

    def __init__(self, coordinates):
        self.coordinates = coordinates
        Vertex.all_vertices.append(self)
        self.normal_ = np.array([0.0, 0, 0])
        self.num_occurences = 0

    def _adjust_normal_vector(self, normal_vector):
        self.normal_ += normal_vector
        self.num_occurences += 1

    def _set_normal(self):
        self.normal_ /= self.num_occurences


class Polygon:

    all_polygons = []

    def __init__(self, points):
        """
        :param points: array-like, vertices
        """
        self.points = points
        Polygon.all_polygons.append(self)
        # plane normal vector
        self.normal_ = None
        first_vec = points[1].coordinates - points[0].coordinates
        second_vec = points[2].coordinates - points[0].coordinates
        # cross product
        self.normal_ = np.cross(first_vec, second_vec)
        # normalization
        self.normal_ /= np.linalg.norm(self.normal_)
        # adjust normal vectors for vertices
        for point in points:
            point._adjust_normal_vector(self.normal_)


def parse_object_file(path):
    """
    :param path:    string - path to file
    :return:        list of all vertices
                    list of all polygons
    """
    all_vertices = Vertex.all_vertices
    file_in = open(path, "r")
    for line in file_in:
        line = line.split()
        if len(line) == 0:
            continue
        if line[0] == 'v':
            Vertex(np.array([float(line[1]), float(line[2]), float(line[3])]))
        elif line[0] == 'f':
            Polygon([all_vertices[int(line[1]) - 1], all_vertices[int(line[2]) - 1], all_vertices[int(line[3]) - 1]])
    for vertex in Vertex.all_vertices:
        vertex._set_normal()
    return Vertex.all_vertices, Polygon.all_polygons


if __name__ == "__main__":
    vertices, polygons = parse_object_file('kocka.obj')
    print(vertices)
    pdb.set_trace()

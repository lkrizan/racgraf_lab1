import numpy as np

def parse_ini(path):
    """
    :param path:    string - path to .ini file
    :return:        dictionary with settings
    """
    point_keys = ['POV', 'L_SOURCE', 'COA']
    pair_keys = ['L_AMBIENT', 'L_DIFFUSE']
    single_keys = ['STEP']
    bool_keys = ['SHADING']
    file_in = open(path, 'r')
    return_values = {}
    key = None
    for line in file_in:
        line = line.split()
        # handling empty lines
        if len(line) == 0:
            continue
        # key handling
        elif line[0][0] == '[':
            key = line[0][1:-1]
            return_values[key] = []
        elif key == 'CURVE_POINTS':
            return_values[key].append(np.array([float(line[0]), float(line[1]), float(line[2])]))
        elif key in single_keys:
            return_values[key] = float(line[0])
        elif key in point_keys:
            return_values[key] = np.array([float(line[0]), float(line[1]), float(line[2])])
        elif key in pair_keys:
            return_values[key] = (float(line[0]), float(line[1]))
        elif key in bool_keys:
            return_values[key] = int(line[0])
        else:
            return_values[key].extend(line)
    return return_values

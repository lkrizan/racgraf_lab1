import sys
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import ini_parser
from object import *
import pdb
from math import acos, degrees
# from copy import deepcopy

# global variables
INI_FILE_PATH = 'rg_lab1.ini'
VALUES = None
vertices = None
polygons = None
control_points = None
points = None
points_d = None
current_index = 0

def display():
    if current_index == len(points):
        exit(0)
    # Clear the color and depth buffers
    glClearColor(1,1,1,1)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    # draw_object()
    draw_axis()
    # draw_control_polygon()
    draw_curve(points, points_d)
    glPushMatrix()
    animate_object()
    glPopMatrix()
    update_perspective()
    glutSwapBuffers()


def draw_control_polygon():
    points = VALUES['CURVE_POINTS']
    glColor3ub(0,255,0)
    glBegin(GL_LINE_STRIP)
    for point in points:
        glVertex3f(*point)
    glEnd()


def draw_axis(length=100):
    glBegin(GL_LINES)
    glColor3ub(255, 0, 255)
    glVertex3f(0, 0, 0)
    glVertex3f(length, 0, 0)
    glVertex3f(0, 0, 0)
    glVertex3f(0, length, 0)
    glVertex3f(0, 0, 0)
    glVertex3f(0, 0, length)
    glEnd()


def calculate_b_curve():
    """
    :return: points, derivations in points
    """
    step = VALUES['STEP']
    points = VALUES['CURVE_POINTS']
    points_d = []
    points_ = []
    # glColor3ub(0,0,0)
    # glBegin(GL_LINE_STRIP)
    for i in range(len(points) - 4):
        t = 0
        while t < 1:
            # spline point calculation
            t_mat = np.array([t**3, t**2, t, 1])
            B = 1/6 * np.array([[-1, 3, -3, 1],
                                [3, -6, 3, 0],
                                [-3, 0, 3, 0],
                                [1, 4, 1, 0]])
            R = np.array([points[i], points[i+1],
                          points[i+2], points[i+3]])
            p = np.dot(t_mat, B)
            p = np.dot(p, R)
            points_.append(p)
            # glVertex3f(*p)
            # derivation calculation
            t_d = np.array([t**2, t, 1])
            B_ = 0.5 * np.array([[-1, 3, -3, 1],
                                [2, -4, 2, 0],
                                [-1, 0, 1, 0]])
            p_d = np.dot(t_d, B_)
            p_d = np.dot(p_d, R)
            # normalize p_d
            p_d = p_d / np.linalg.norm(p_d)
            points_d.append(p_d)
            # advance loop
            t += step
    # glEnd()
    return points_, points_d


def draw_curve(points, points_d, draw_derivatives=True):
    # curve
    glColor3ub(0,0,0)
    glBegin(GL_LINE_STRIP)
    for point in points:
        glVertex3f(*point)
    glEnd()
    # derivations
    if draw_derivatives:
        glColor3ub(0, 255, 0)
        glBegin(GL_LINES)
        for i in range(1, len(points), 15):
            p = points[i]
            p_d = points_d[i]
            glVertex3f(*p)
            glVertex3f(*((p + p_d * 5)))
        glEnd()


def update_perspective():
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, 640/480, 0.1, 10000)
    gluLookAt(*VALUES['POV'], *VALUES['COA'], 0, 1, 0)
    glMatrixMode(GL_MODELVIEW)


def draw_object():
    for polygon in polygons:
        """
        if not visible(polygon):
            continue
        """
        glBegin(GL_LINE_LOOP)
        glColor3ub(0, 0, 0)
        glVertex3f(*polygon.points[0].coordinates)
        glVertex3f(*polygon.points[1].coordinates)
        glVertex3f(*polygon.points[2].coordinates)
        glEnd()


def draw_shaded_object():
    (I_a, k_a) = VALUES['L_AMBIENT']
    (I_i, k_d) = VALUES['L_DIFFUSE']
    source = VALUES['L_SOURCE']
    for polygon in polygons:
        """
        if not visible(polygon):
            continue
        """
        glBegin(GL_TRIANGLES)
        for vertex in polygon.points:
            # vector from vertex to light source
            vec = (source - vertex.coordinates) / np.linalg.norm(source - vertex.coordinates)
            ln = np.dot(vertex.normal_, vec)
            if ln < 0:
                ln = 0
            I = int(I_a * k_a + I_i * k_d * ln)
            glColor3ub(I, I, I)
            glVertex3f(*vertex.coordinates)
        glEnd()

def animate_object(step=2):
    global current_index
    s = np.array([0, 0, 1])
    # translation
    glTranslatef(*points[current_index])
    # rotation
    rotation_angle = degrees(acos(np.dot(s, points_d[current_index])))
    rotation_axis = np.cross(s, points_d[current_index])
    # s = points_d[current_index]
    glRotatef(rotation_angle, *rotation_axis)
    if VALUES["SHADING"]:
        draw_shaded_object()
    else:
        draw_object()
    current_index += step

def visible(polygon):
    D = np.dot(polygon.points[0].coordinates, polygon.normal_)
    if np.dot(VALUES['POV'], polygon.normal_) + D < 0:
        return False
    return True


def scale_and_move(points):
    """
    :param points: Vertex, contains np.array objects
    """
    pi = np.array([vertex.coordinates for vertex in points])
    max_ = np.max(pi, axis=0)
    min_ = np.min(pi, axis=0)
    center = (max_ + min_) / 2
    max_size = np.max(max_ - min_)
    for point in points:
        point.coordinates -= center
        point.coordinates *= 6 / max_size

previous_t = 0
def idle():
    global previous_t
    current_t = glutGet(GLUT_ELAPSED_TIME)
    t_interval = current_t - previous_t
    if t_interval > 10:
        display()
        previous_t = current_t


if __name__ == "__main__":
    # glut initialization
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
    glutInitWindowSize(640, 480)
    window = glutCreateWindow('Pracenje putanje')
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LEQUAL)
    glutDisplayFunc(display)
    glutIdleFunc(idle)
    # data initialization
    VALUES = ini_parser.parse_ini(INI_FILE_PATH)
    vertices, polygons = parse_object_file(VALUES['OBJECT'][0])
    control_points = np.array(VALUES['CURVE_POINTS'])
    scale_and_move(vertices)
    points, points_d = calculate_b_curve()
    glutMainLoop()
    """
    TO DO:
        - animacija objekta
    """
